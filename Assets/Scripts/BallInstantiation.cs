﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BallInstantiation : MonoBehaviour
{
    public float animspeed;
    GameObject BallInit;
    public GameObject BallInitPrefab;
    public float thrust;
    public float ReplayThrust;
    public Transform BallTransform;
    Rigidbody rb;
    public float min;
    public float max;
    public float newmin;
    public float newmax;
    public bool isBallInit;
    public bool isPlaying;
    public bool IsReplay = false;
    public GameObject BallParent;
    public Slider thrustReductionslider;
    public Text thrustReductionslidertext;
    public GameObject EnvironmentScale;
    private float rotationsPerMinute = 1000;
    public float ballscale;
    void Update()
    {
        this.GetComponent<Animator>().speed = animspeed ;
        //BallInit.transform.Rotate((float)(rotationsPerMinute * Time.deltaTime), 0, 0);
        Time.timeScale = thrustReductionslider.value;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }

    public void TestSpeed()
    {
            if (IsReplay)
            {
                thrust = ReplayThrust;
                IsReplay = !IsReplay;
            }
            else
            {
            newmin = (min * (EnvironmentScale.transform.localScale.x * thrustReductionslider.value));
            newmax = (max * (EnvironmentScale.transform.localScale.x * thrustReductionslider.value));
            //thrust = -Random.Range(min * ( EnvironmentScale.transform.localScale.x * thrustReductionslider.value), max * (EnvironmentScale.transform.localScale.x * thrustReductionslider.value));
            thrust = -Random.Range(min, max);
            ReplayThrust = thrust;
            }
            
            BallInit = Instantiate(BallInitPrefab, new Vector3(BallTransform.position.x, BallTransform.position.y, BallTransform.position.z), Quaternion.identity);
            BallInit.transform.parent = BallParent.transform;
            //BallInit.transform.localScale = BallInitPrefab.transform.lossyScale;
            rb = BallInit.GetComponent<Rigidbody>();
            rb.AddForce(0, 0, thrust, ForceMode.Force);
            //BallInit.transform.Rotate((float)(rotationsPerMinute * Time.deltaTime), 0,0);
            //rb.maxAngularVelocity = 100;
            //rb.angularVelocity = Vector3.Cross(transform.up, transform.right) * transform.right.magnitude * Mathf.Deg2Rad;
            thrustReductionslidertext.text = EnvironmentScale.transform.localScale.x.ToString();
            isBallInit = true;
    }
   
}
