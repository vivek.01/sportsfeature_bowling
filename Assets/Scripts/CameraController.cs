using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public List<Transform> m_DropOptions_Cameraposition = new List<Transform>();
    public Camera MainCamera;
    GameObject[] MidWicketBoxGameObject;
    GameObject[] OffSideBoxGameObject;
    GameObject[] LegSideBoxGameObject;
    private void Start()
    {
        MidWicketBoxGameObject = GameObject.FindGameObjectsWithTag("MidWicketBox");
        OffSideBoxGameObject = GameObject.FindGameObjectsWithTag("OffSideBox");
        LegSideBoxGameObject = GameObject.FindGameObjectsWithTag("LegSideBox");
    }
    public void DropDownFun(int value)
    {
        if (value == 2)
        {
            MainCamera.transform.localPosition = m_DropOptions_Cameraposition[2].transform.position;
            MainCamera.transform.localRotation = m_DropOptions_Cameraposition[2].transform.localRotation;
            foreach (var item in MidWicketBoxGameObject)
            {
                if(item.transform.GetChild(1).GetComponent<BowlingInfographics>().Ballhit == false)
                {
                    item.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
                    item.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
                }
            }
            foreach (var item in OffSideBoxGameObject)
            {
                if (item.transform.GetChild(1).GetComponent<BowlingInfographics>().Ballhit == false)
                {
                    item.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
                    item.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
                }
            }
            foreach (var item in LegSideBoxGameObject)
            {
                if (item.transform.GetChild(1).GetComponent<BowlingInfographics>().Ballhit == false)
                {
                    item.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
                    item.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
                }
            }
        }
        if (value == 1)
        {
            MainCamera.transform.localPosition = m_DropOptions_Cameraposition[1].transform.position;

            MainCamera.transform.localRotation = m_DropOptions_Cameraposition[1].transform.localRotation;
            foreach (var item in MidWicketBoxGameObject)
            {
                if (item.transform.GetChild(1).GetComponent<BowlingInfographics>().Ballhit == false)
                {
                    item.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
                    item.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
                }
            }
            foreach (var item in OffSideBoxGameObject)
            {
                if (item.transform.GetChild(1).GetComponent<BowlingInfographics>().Ballhit == false)
                {
                    item.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
                    item.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
                }
            }
            foreach (var item in LegSideBoxGameObject)
            {
                if (item.transform.GetChild(1).GetComponent<BowlingInfographics>().Ballhit == false)
                {
                    item.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
                    item.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
                }
            }
        }
        if (value == 0)
        {
            MainCamera.transform.localPosition = m_DropOptions_Cameraposition[0].transform.position;

            MainCamera.transform.localRotation = m_DropOptions_Cameraposition[0].transform.localRotation;
            foreach (var item in MidWicketBoxGameObject)
            {
               
                    item.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                    item.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;
            }
            foreach (var item in OffSideBoxGameObject)
            {
                    item.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                    item.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;
            }
            foreach (var item in LegSideBoxGameObject)
            {
                    item.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                    item.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;
            }
        }
    }
}
