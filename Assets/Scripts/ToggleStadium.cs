﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ToggleStadium : MonoBehaviour
{
    public Toggle stadium_Toggle;
    public GameObject stadium1;
    public Toggle ground_Toggle;
    public Toggle infographic_wall_Toggle;
    public Toggle ballType_Toggle;
    public Toggle pitchMap_Toggle;
    public GameObject ground;
    public GameObject infographic_wall;
    public GameObject ballType;
    public GameObject pitchMap;
    public void ToggleStadiums()
    {
        if (stadium_Toggle.isOn)
        {
            stadium1.SetActive(true);
        }
        else
        {
           stadium1.SetActive(false);
        }
    }
    public void ToggleGround()
    {
        if (ground_Toggle.isOn)
        {
            ground.SetActive(true);
        }
        else
        {
            ground.SetActive(false);
        }
    }
    public void Toggleinfographic_wall()
    {
        if (infographic_wall_Toggle.isOn)
        {
            infographic_wall.SetActive(true);
        }
        else
        {
            infographic_wall.SetActive(false);
        }
    }
    public void ToggleballType()
    {
        if (ballType_Toggle.isOn)
        {
            ballType.SetActive(true);
        }
        else
        {
            ballType.SetActive(false);
        }
    }
    public void TogglepitchMap()
    {
        if (pitchMap_Toggle.isOn)
        {
            pitchMap.SetActive(true);
        }
        else
        {
            pitchMap.SetActive(false);
        }
    }

}
