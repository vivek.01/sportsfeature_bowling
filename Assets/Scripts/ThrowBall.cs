using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowBall : MonoBehaviour
{
    public GameObject Yuvraj_bowling_animation;
    public void ThrowNextBall()
    {
        Yuvraj_bowling_animation.GetComponent<Animator>().Play("YUV Bowling Anim", -1, 0f);
    }
}
