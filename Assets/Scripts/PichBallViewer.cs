﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PichBallViewer : MonoBehaviour
{
    public GameObject pitchBall;
    public GameObject pitchBallinitantiated;
    public Transform pitchBallinitantiatedParent;

    private void OnCollisionEnter(Collision collision)
    {
        var pointofcollision = collision.GetContact(0).point;
        pitchBallinitantiated = Instantiate(pitchBall, pointofcollision, Quaternion.identity, pitchBallinitantiatedParent);
    }
}
