using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetScene : MonoBehaviour
{
    //public List<GameObject> CricketballGameObject = new List<GameObject>();
    //public List<GameObject> InfographicsballGameObject = new List<GameObject>();
    //public List<GameObject> PitchBallHighlitersGameObject = new List<GameObject>();
    public void Cricketballfun()
    {
        GameObject[] CricketballGameObject = GameObject.FindGameObjectsWithTag("Cricket ball");
        foreach (var item in CricketballGameObject)
        {
            Destroy(item);
        }
    }
    public void InfographicsballParentfun()
    {
        GameObject[] InfographicsballParentGameObject = GameObject.FindGameObjectsWithTag("Infographics ball Parent");
        foreach (var item in InfographicsballParentGameObject)
        {
            Destroy(item);
        }
    }

    public void PitchBallHighlitersparentfun()
    {
        GameObject[] PitchBallHighlitersparentGameObject = GameObject.FindGameObjectsWithTag("PitchBallHighliters parent");
        foreach (var item in PitchBallHighlitersparentGameObject)
        {
            Destroy(item);
        }
    }
}
