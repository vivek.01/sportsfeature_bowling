using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitchController : MonoBehaviour
{
    public List<GameObject> m_DropOptions = new List<GameObject>();
    public void DropDownFun(int value)
    {
        if(value == 0)
        {
            foreach (var item in m_DropOptions)
            {
                item.SetActive(false);
            }
            m_DropOptions[0].SetActive(true);
        }
        if (value == 1)
        {
            foreach (var item in m_DropOptions)
            {
                item.SetActive(false);
            }
            m_DropOptions[1].SetActive(true);
        }
        if (value == 2)
        {
            foreach (var item in m_DropOptions)
            {
                item.SetActive(false);
            }
            m_DropOptions[2].SetActive(true);
        }
        if (value == 3)
        {
            foreach (var item in m_DropOptions)
            {
                item.SetActive(false);
            }
            m_DropOptions[3].SetActive(true);
        }
        if (value == 4)
        {
            foreach (var item in m_DropOptions)
            {
                item.SetActive(false);
            }
            m_DropOptions[4].SetActive(true);
        }

    }
}
