using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTypeLengthOpacity : MonoBehaviour
{
    Color alpha;
    private void OnTriggerEnter(Collider other)
    {
        gameObject.GetComponent<AudioSource>().Play();
        alpha  = gameObject.GetComponent<MeshRenderer>().material.color;
        Debug.Log("coroutine called" + gameObject.name + "Alpha : ---"+ alpha);
        alpha.a = 1f ;
        //Debug.Log("coroutine called" + gameObject.name + "Alpha : ---" + alpha.a);
        gameObject.GetComponent<MeshRenderer>().material.color = alpha;
        StartCoroutine(BacktoTransperent());
    }
    IEnumerator BacktoTransperent()
    {
        yield return new WaitForSeconds(2f);
        //Debug.Log("coroutine called"+gameObject.name);
        alpha.a = 0.3f;
        gameObject.GetComponent<MeshRenderer>().material.color = alpha;
    }
}
