﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class BowlingInfographics : MonoBehaviour
{

    public GameObject ball;
    public GameObject ballinitantiated;
    public Transform ballinitantiatedParent;
    public Material highlitedMaterial;
    public GameObject default_gameObject;
    public bool Ballhit = false;
    private float speed = 2000f;
    public bool tile_Movement = false;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(" Entered trigger" + gameObject.name);
        
        ballinitantiated = Instantiate(ball,  other.transform.position, Quaternion.identity, ballinitantiatedParent);
        gameObject.GetComponent<Renderer>().material = highlitedMaterial;
        default_gameObject.transform.Translate(0, 0, -0.1f);
        //Invoke("BacktoOrigin", 1.0f);
        StartCoroutine(BacktoOrigin());
        Ballhit = true;
    }
    IEnumerator BacktoOrigin()
    {
        yield return new WaitForSeconds(1f);
        tile_Movement = true;
        //Debug.Log("coroutine called");

        //default_gameObject.transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(0, 0, 0), step); //boolean vector3.dist
        //default_gameObject.transform.localPosition = new  Vector3(0,0,0);
    }
    private void Update()
    {
        if(tile_Movement)
        {
            float step = speed;
            default_gameObject.transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(0, 0, 0), step * Time.deltaTime);
            tile_Movement = false;
        }
    }
}
