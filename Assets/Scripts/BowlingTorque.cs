﻿using UnityEngine;

public class BowlingTorque : MonoBehaviour
{
    public float torqueAmount = 500f;
    public float ForceAmount = 100f;
    public float rotationsPerMinute = 500f;

    // Update is called once per frame
    void FixedUpdate()
    {
        gameObject.GetComponent<Rigidbody>().AddTorque(-transform.right * torqueAmount);
        //gameObject.transform.Rotate((float)(rotationsPerMinute ), 0, 0);
        //gameObject.GetComponent<TrailRenderer>().startWidth = 0.001f;
        //gameObject.GetComponent<TrailRenderer>().endWidth = 0.001f;

    }
    private void OnCollisionEnter(Collision collision)
    {
        gameObject.GetComponent<Rigidbody>().AddForce(transform.up * ForceAmount);
    }
}
