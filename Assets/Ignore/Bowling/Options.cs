﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Options : MonoBehaviour
{
    public List<Button> buttonPressedList = new List<Button>();
    public GameObject Yuvi;
    public Canvas QuestionCan;
    public BallInstantiation ballInstantiation;
    private Length length;

    private void Start()
    {
        GetLengthName.OnBallHit += SetLength;
    }
    public void OnLenghtButtonClick(Button buttonLenghtType)
    {
        buttonPressedList.Add(buttonLenghtType);
        if (length == buttonLenghtType.GetComponent<EnumLength>().length)
        {
            ChangeColor(buttonLenghtType, Color.green);
        }
        else
        {
            ChangeColor(buttonLenghtType, Color.red);
        }
    }

    public void SetLength(Length l)
    {
        Debug.Log(l);
        length = l;
    }
    public void Replay_fun(Button buttonLenghtType)
    {
        QuestionCan.enabled = false;
        Yuvi.SetActive(true);
        ballInstantiation.IsReplay = true;
        ResetButtonColor();
    }

    public void Next_fun(Button buttonLenghtType)
    {
        QuestionCan.enabled = false;
        Yuvi.SetActive(true);
        ResetButtonColor();
    }
    
    public void ResetButtonColor()
    {
        foreach (var item in buttonPressedList)
        {
            ChangeColor(item, Color.white);
        }
    }
   
    public void ChangeColor(Button buttonName, Color color)
    {
        buttonName.GetComponent<Image>().color = color;
    }
 
}
