﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initball : MonoBehaviour
{
    GameObject BallInit;
    public GameObject BallInitPrefab;
    public static float thrust;
    public Transform BallTransform;
    Rigidbody rb;
    public int min;
    public int max;
    
    public bool isBallInit;

    public float speedofangularvel = 10f;
    private float rotationsPerMinute = 300;

    private void OnCollisionEnter(Collision collision)
    {
        
        
    }
    private void OnTriggerExit(Collider other)
    {
        //Debug.Log("Init Ball");
        if (!isBallInit)
        {
            thrust = -Random.Range(min, max);
            Debug.Log("Init Ball collision");
            //BallInit = Instantiate(BallInitPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            BallInit = Instantiate(BallInitPrefab, new Vector3(BallTransform.position.x, BallTransform.position.y, BallTransform.position.z), Quaternion.identity);
            BallInit.transform.parent = gameObject.transform;
            //BallInit.transform.Rotate(0, (float)(6.0 * rotationsPerMinute * Time.deltaTime), 0);
            rb = BallInit.GetComponent<Rigidbody>();
            rb.AddForce(0, 0, thrust, ForceMode.Impulse);
            //rb.sleepThreshold = 150f;
            //rb.angularVelocity = (Vector3.Cross(transform.position, transform.right)* transform.right.magnitude* Mathf.Deg2Rad);
            //rb.maxAngularVelocity = 1000f;
            //rb.AddTorque(new Vector3(0, 0, 1000), ForceMode.Force);
            isBallInit = true;
        }
        //BallInit = Instantiate(BallInitPrefab, BallTransform);

    }
    private void FixedUpdate()
    {
        
            //BallInit.GetComponent<Rigidbody>().AddForce(-transform.forward * thrust);
       

    }
}
