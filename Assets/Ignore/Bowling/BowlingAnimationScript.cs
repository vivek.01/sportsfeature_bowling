﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowlingAnimationScript : MonoBehaviour
{
    GameObject BallInit;
    public GameObject BallInitPrefab;
    public float thrust;
    public float ReplayThrust;
    public Transform BallTransform;
    Rigidbody rb;
    public int min;
    public int max;
    public bool isBallInit;
    public bool isPlaying;
    public bool IsReplay = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
   
    private void OnTriggerExit(Collider other)
    {
        //Debug.Log("Init Ball");
        if (!isBallInit)
        {
            if (IsReplay)
            {
                thrust = ReplayThrust;
                IsReplay = !IsReplay;
            }
            else
            {
                thrust = -Random.Range(min, max);
                ReplayThrust = thrust;
            }
                Debug.Log("Init Ball collision");
                BallInit = Instantiate(BallInitPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
                BallInit.transform.parent = gameObject.transform;
                rb = BallInit.GetComponent<Rigidbody>();
                rb.AddForce(0, 0, thrust, ForceMode.Impulse);
                isBallInit = true;
            
        }
       
    }
    
}
