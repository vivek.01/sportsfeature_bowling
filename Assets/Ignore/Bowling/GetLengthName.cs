﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetLengthName : MonoBehaviour
{
    public GameObject Yuvi;
    public Canvas QuestionCan;
    public Length length;
    public static Action<Length> OnBallHit;

    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            Invoke("CallQuestion",2f);
        }
        OnBallHit(length);
    }

    private void CallQuestion()
    {
        QuestionCan.enabled = true;
        Yuvi.SetActive(false);
    }
    
}
