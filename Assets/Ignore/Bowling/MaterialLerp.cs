﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialLerp : MonoBehaviour
{
    public Material material1;
    public Material material2;
    public float t;
    public float duration =2;

    // Start is called before the first frame update
    void Start()
    {
        //gameObject.GetComponent<Renderer>().material.Lerp(material1, material2, t);
    }

    // Update is called once per frame
    void Update()
    {
        float t = Mathf.PingPong(Time.time, duration) / duration;
        gameObject.GetComponent<Renderer>().material.Lerp(material1, material2, t);
    }
}
