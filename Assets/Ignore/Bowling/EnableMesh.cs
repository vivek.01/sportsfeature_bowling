﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableMesh : MonoBehaviour
{
    // Start is called before the first frame update
    public Initball m_initball;
    public static string lengthName;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            
            Debug.Log("mesh enable");
            gameObject.GetComponent<MeshRenderer>().enabled = true;
            StartCoroutine(Wait());
            lengthName = gameObject.name;
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(3f);
        Debug.Log("wait");
        DisableMesh();
    }
    void DisableMesh()
    {
        m_initball.isBallInit = false;
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        Debug.Log("mesh disable");
    }
}
