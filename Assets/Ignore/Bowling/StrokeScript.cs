﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StrokeScript : MonoBehaviour
{
    public List<GameObject> length = new List<GameObject>();
    public List<GameObject> fullHalfVolley = new List<GameObject>();
    public List<GameObject> goodLength = new List<GameObject>();
    public List<GameObject> shortofaLength = new List<GameObject>();
    public List<GameObject> shortLength = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void FullHalfVolley()
    {
        foreach (GameObject item in length)
        {
            item.GetComponent<Button>().interactable = true;
        }
        foreach (GameObject item in fullHalfVolley)
        {
            item.GetComponent<Button>().interactable = true;
        }
        foreach (GameObject item in goodLength)
        {
            item.GetComponent<Button>().interactable = false;
        }
        foreach (GameObject item in shortofaLength)
        {
            item.GetComponent<Button>().interactable = false;
        }
        foreach (GameObject item in shortLength)
        {
            item.GetComponent<Button>().interactable = false;
        }
        length[0].GetComponent<Button>().interactable = false;
    }
    public void GoodLength()
    {
        foreach (GameObject item in length)
        {
            item.GetComponent<Button>().interactable = true;
        }
        foreach (GameObject item in fullHalfVolley)
        {
            item.GetComponent<Button>().interactable = false;
        }
        foreach (GameObject item in goodLength)
        {
            item.GetComponent<Button>().interactable = true;
        }
        foreach (GameObject item in shortofaLength)
        {
            item.GetComponent<Button>().interactable = false;
        }
        foreach (GameObject item in shortLength)
        {
            item.GetComponent<Button>().interactable = false;
        }
        length[1].GetComponent<Button>().interactable = false;
    }
    public void ShortofaLength()
    {
        foreach (GameObject item in length)
        {
            item.GetComponent<Button>().interactable = true;
        }
        foreach (GameObject item in fullHalfVolley)
        {
            item.GetComponent<Button>().interactable = false;
        }
        foreach (GameObject item in goodLength)
        {
            item.GetComponent<Button>().interactable = false;
        }
        foreach (GameObject item in shortofaLength)
        {
            item.GetComponent<Button>().interactable = true;
        }
        foreach (GameObject item in shortLength)
        {
            item.GetComponent<Button>().interactable = false;
        }
        length[2].GetComponent<Button>().interactable = false;
    }
    public void Short()
    {
        foreach (GameObject item in length)
        {
            item.GetComponent<Button>().interactable = true;
        }
        foreach (GameObject item in fullHalfVolley)
        {
            item.GetComponent<Button>().interactable = false;
        }
        foreach (GameObject item in goodLength)
        {
            item.GetComponent<Button>().interactable = false;
        }
        foreach (GameObject item in shortofaLength)
        {
            item.GetComponent<Button>().interactable = false;
        }
        foreach (GameObject item in shortLength)
        {
            item.GetComponent<Button>().interactable = true;
        }
        length[3].GetComponent<Button>().interactable = false;
    }
    public void CoverDrive()
    {
        foreach (GameObject item in fullHalfVolley)
        {
            item.GetComponent<Button>().image.color = Color.white;
        }
        fullHalfVolley[0].GetComponent<Button>().image.color = Color.blue;
    }
    public void StraightOffDrive()
    {
        foreach (GameObject item in fullHalfVolley)
        {
            item.GetComponent<Button>().image.color = Color.white;
        }
        fullHalfVolley[1].GetComponent<Button>().image.color = Color.blue;
    }
    public void OnDriveGlance()
    {
        foreach (GameObject item in fullHalfVolley)
        {
            item.GetComponent<Button>().image.color = Color.white;
        }
        fullHalfVolley[2].GetComponent<Button>().image.color = Color.blue;
    }
    public void ForwardDefenceSweep()
    {
        foreach (GameObject item in goodLength)
        {
            item.GetComponent<Button>().image.color = Color.white;
        }
        goodLength[0].GetComponent<Button>().image.color = Color.blue;
    }
     public void BackFootGlance()
     {
        foreach (GameObject item in shortofaLength)
        {
            item.GetComponent<Button>().image.color = Color.white;
        }
        shortofaLength[0].GetComponent<Button>().image.color = Color.blue;
    }
     public void BackFootdefencedrive()
     {
        foreach (GameObject item in shortofaLength)
        {
            item.GetComponent<Button>().image.color = Color.white;
        }
        shortofaLength[1].GetComponent<Button>().image.color = Color.blue;
    }
     public void LateCut()
     {
        foreach (GameObject item in shortofaLength)
        {
            item.GetComponent<Button>().image.color = Color.white;
        }
        shortofaLength[2].GetComponent<Button>().image.color = Color.blue;
    }
     public void Pull()
     {
        foreach (GameObject item in shortLength)
        {
            item.GetComponent<Button>().image.color = Color.white;
        }
        shortLength[0].GetComponent<Button>().image.color = Color.blue;
    }
     public void SquareCut()
     {
        foreach (GameObject item in shortLength)
        {
            item.GetComponent<Button>().image.color = Color.white;
        }
        shortLength[1].GetComponent<Button>().image.color = Color.blue;
    }
     public void Hook()
     {
        foreach (GameObject item in shortLength)
        {
            item.GetComponent<Button>().image.color = Color.white;
        }
        shortLength[2].GetComponent<Button>().image.color = Color.blue;
    }
     
}
