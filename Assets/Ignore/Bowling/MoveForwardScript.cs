﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForwardScript : MonoBehaviour
{
    Rigidbody m_Rigidbody;
    [SerializeField]
    private float m_Speed;
    [SerializeField]
    private bool startMoving;
    float step;
    Vector3 myposition;
    // Start is called before the first frame update
    void Start()
    {
        //Fetch the Rigidbody component you attach from your GameObject
        m_Rigidbody = GetComponent<Rigidbody>();
        myposition = gameObject.transform.position;
    }
    public void StartMoving ()
    {
        startMoving = true;
        m_Speed = 2.5f;
    }
    public void StopMoving()
    {
        startMoving = false;
        m_Speed = 0f;
    }
    public void OriginalPosition()
    {
        gameObject.transform.position = myposition;
    }
    // Update is called once per frame
    void Update()
    {
        if (startMoving)
        {
            step = m_Speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, transform.forward, step);
            //Move the Rigidbody forwards constantly at speed you define (the blue arrow axis in Scene view)
            //m_Rigidbody.velocity = transform.forward * m_Speed;
        }
    }
}
