﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitCoRoutine());
        fun1();
    }
    IEnumerator  TestCoRoutine()
    {
        yield return (StartCoroutine(WaitCoRoutine()));
        // fun2();
        fun3();
    }
    void fun1()
    {
        Debug.Log("Fun 1 called");

    }
    void fun2()
    {

        Debug.Log("Fun 2 called");
    }

    void fun3()
    {

        Debug.Log("Fun 3 called");
    }


    IEnumerator WaitCoRoutine()
    {
        //Debug.Log("Wait 1 called");
        yield return new WaitForSeconds(5f);
        //Debug.Log("ait 2 called");
        //fun2();
    }
}
