﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnumLength : MonoBehaviour
{
    public Length length;
}
public enum Length
{
    FullHalfVolley,
    GoodLength,
    ShortOfAGoodLength,
    Short
}