﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class OnValidateScript : MonoBehaviour
{
    [SerializeField]
    private Options rigid = null;
    public OnValidateExample game;
    private Image image;
    private void OnValidate()
    {
        if (rigid == null)
        {
            rigid = FindObjectOfType<Options>();
            image.sprite = game.image.sprite;
            Debug.Log("Called Validate");

        }
    }
}
