﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dropdown_color_Script : MonoBehaviour
{
    public List<Image> Dropdown_button = new List<Image>();
    // Start is called before the first frame update
    void Start()
    {
        Dropdown_button[0].GetComponent<Image>().color = new Color32(232, 84, 32, 255);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DropdownButton0()
    {
        for (int i=0; i<Dropdown_button.Count;i++)
        {
            Dropdown_button[i].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
        Dropdown_button[0].GetComponent<Image>().color = new Color32(232, 84, 32, 255);
    }
    public void DropdownButton1()
    {
        for (int i = 0; i < Dropdown_button.Count; i++)
        {
            Dropdown_button[i].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
        Dropdown_button[1].GetComponent<Image>().color = new Color32(232, 84, 32, 255);
    }
    public void DropdownButton2()
    {
        for (int i = 0; i < Dropdown_button.Count; i++)
        {
            Dropdown_button[i].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
        Dropdown_button[2].GetComponent<Image>().color = new Color32(232, 84, 32, 255);
    }
    public void DropdownButton3()
    {
        for (int i = 0; i < Dropdown_button.Count; i++)
        {
            Dropdown_button[i].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
        Dropdown_button[3].GetComponent<Image>().color = new Color32(232, 84, 32, 255);
    }
    public void DropdownButton4()
    {
        for (int i = 0; i < Dropdown_button.Count; i++)
        {
            Dropdown_button[i].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
        Dropdown_button[4].GetComponent<Image>().color = new Color32(232, 84, 32, 255);
    }
    public void DropdownButton5()
    {
        for (int i = 0; i < Dropdown_button.Count; i++)
        {
            Dropdown_button[i].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
        Dropdown_button[5].GetComponent<Image>().color = new Color32(232, 84, 32, 255);
    }

}
