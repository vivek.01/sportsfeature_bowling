﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitchLengthBallColor : MonoBehaviour
{
    public Material pitchLengthColorMaterial;

    public PichBallViewer pichBallViewer;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(" Entered trigger" + gameObject.name);
        pichBallViewer.pitchBallinitantiated.GetComponent<Renderer>().material = pitchLengthColorMaterial;
        gameObject.GetComponent<AudioSource>().Play();
    }
}
